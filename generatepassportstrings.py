# https://en.wikipedia.org/wiki/Machine-readable_passport

import random, string
from corpus.givennames import * # Indonesian given names
from corpus.countrycodes import * # in case we need random countries to train for
from corpus.surnames import * # Indonesian surnames

name_length = [0.5] + [5]*8 + [3]*10 + [1]*10 + [0.5]*8
name_length_sum = sum(name_length)
name_length_weights =  [x/name_length_sum for x in name_length]
passport_types = ["<"] # more types can be added here if dealing with different passport types
possible_number_of_names = [x + 2 for x in range(2)]
possible_months = ["0" + str(x) for x in range(1,10)] + ["10", "11", "12"]
possible_days = possible_months + [str(x) for x in range(13, 32)]
possible_years = possible_days + [str(x) for x in range(32, 100)] + ["00"]
possible_sexes = ["M", "F", "<"]

def make_random_name(length):
    return ''.join(random.choices(string.ascii_uppercase, k = length))
def make_random_passport_number():
    l = random.randrange(4,10) # random length of number, at least 4 (just a guess), at most 9
    number = ''.join(random.choices(string.ascii_uppercase + string.digits, k = l))
    return number + ("<" * (9 - l)) # fill the rest up with <
def make_plausible_passport_number():
    p_number = random.choice(string.ascii_uppercase)
    for i in range(7):
        p_number += random.choice(string.digits)
    return p_number + "<"
def check_digit(s):
    weights = [7,3,1]
    i = 0
    accumulator = 0
    for c in s:
        o = ord(c)
        val = 0
        if o < 58: # digit
            val = o - 48
        elif o < 61: # filler <
            val = 0
        else: # letter
            val = 10 + o - 65
        accumulator += weights[i%3] * val
        i += 1 # cycle through the weights
    return str(accumulator % 10)
def make_date():
    return random.choice(possible_years) + random.choice(possible_months) + random.choice(possible_days)
def make_personal_number():
    if bool(random.getrandbits(1)): # might be usually non-existant
        return "<"*15
    else:
        l = random.randrange(3,15) # random length of number, at least 3 (just a guess), at most 14
        number = ''.join(random.choices(string.ascii_uppercase + string.digits, k = l))
        number += ("<" * (14 - l)) # fill the rest up with <
        return number + check_digit(number) # append check digit

def line1ofpassport():
    # line1 = "P<" + random.choice(country_codes)
    line1 = "P<IDN" 
#        line1 += make_random_name(random.choices(range(1,38), weights=name_length_weights, k=1)[0]) + "<<" # make a random surname that leaves room for 2 filler character and one character given name
    line1 += random.choice(surnames) + "<<" # make a random surname that leaves room for 2 filler character and one character given name
    number_of_names = random.choice(possible_number_of_names) # pick a random number of names to add
    while number_of_names > 0 and len(line1) < 42: # keep adding random names until space runs out or counter reaches 0
        number_of_names -= 1
        space_left = 43 - len(line1)
        tries = 5
        while tries > 0:
            tries -= 1
            random_name = random.choice(given_names)
            if len(random_name) < space_left:
                line1 += random_name + "<"
                break
    space_left = 44 - len(line1)
    line1 += "<" * space_left
#        print(line1)
    return line1
def line2ofpassport():
    # passport_number = make_random_passport_number()
    passport_number = make_plausible_passport_number()
    line2 = passport_number + check_digit(passport_number)
    # line2 += random.choice(country_codes) # not random now because only considering IDN passwords
    line2 += "IDN"
    dob = make_date()
    line2 += dob + check_digit(dob)
    line2 += random.choice(possible_sexes)
    expiration_date = make_date()
    line2 += expiration_date + check_digit(expiration_date)
    # # skip for now since personal numbers seem not existent in Indonesian passports
    line2 += make_personal_number()
    # so fill with filler characters
    # line2 += "<"*(43 - len(line2))
    line2 += check_digit(line2[0:10] + line2[13:20] + line2[21:43])
    # print(line2)
    return line2    
def line1or2ofpassport():
    if bool(random.getrandbits(1)): # generate a random 1st passport line
        return line1ofpassport()
    else: # generate a random 2nd passport line
        return line2ofpassport()

if __name__ == "__main__":
    print("writing training data")
    with open("passportline1strings_train.txt", "w") as f:
        for _ in range(64000):
            f.write(line1ofpassport() + "\n")
    with open("passportline2strings_train.txt", "w") as f:
        for _ in range(64000):
            f.write(line2ofpassport() + "\n")
    print("done")
    print("writing eval data")
    with open("passportline1strings_eval.txt", "w") as f:
        for _ in range(16000):
            f.write(line1ofpassport() + "\n")
    with open("passportline2strings_eval.txt", "w") as f:
        for _ in range(16000):
            f.write(line2ofpassport() + "\n")
    print("everything done")
