from corpus.provinces import *
from corpus.givennames import *
from corpus.surnames import *
from corpus.streetnames import *
import random, string


possible_months = ["0" + str(x) for x in range(1,10)] + ["10", "11", "12"]
possible_days = possible_months + [str(x) for x in range(13, 32)]
possible_years = possible_days + [str(x) for x in range(32, 100)] + ["00"]
possible_sexes = ["LAKI-LAKI", "BINI-BINI"]
possible_number_of_names = [x + 2 for x in range(2)]
blood_types = ['-', 'A', 'B', 'AB', 'O', 'A+', 'B+', 'AB+', 'O+']
possible_religions = ['ISLAM', 'KRISTEN', 'HINDU', 'BUDDHA', 'KHONGHUCU']
possible_marital_status = ['BELUM KAWIN', 'KAWIN', 'CERAI HIDUP', 'CERAI MATI']

def getProvince():
    return f'PROVINSI {random.choice(provinces_of_indonesia).upper()}'
def getCity():
    city = random.choice(cities_of_indonesia)
    if city[:4] == 'KAB.' and bool(random.getrandbits(1)):
        city = f'KABUPATEN{city[4:]}'
    return city
def getNIK():
    return ''.join(random.choices(string.digits, k=16))
def getNIKString():
    return f'NIK     : {getNIK()}'
def getNIK_r():
    c = random.choices([1,2,3], weights=[20,20,1],k=1)[0]
    if c == 1: 
        return 'NIK'
    elif c == 2:
        return f': {getNIK()}'
    else:
        return getNIKString()
def getName():
    name = random.choice(given_names)
    for _ in range(random.choice(possible_number_of_names) - 1):
        name = f'{name} {random.choice(surnames)}'
    return f':{name}'
def getNameString():
    return f'Nama      {getName()}'
def getName_r():
    c = random.choices([1,2], weights=[1,20],k=1)[0]
    # c = random.choices([1,2,3], weights=[1,10,10],k=1)[0]
    if c == 1: 
        return 'Nama'
    # elif c == 2:
    #     return getNameString()
    else:
        return getName()
def getDate():
    return '-'.join([random.choice(possible_days), random.choice(possible_months), random.choice(possible_years)])
def getShortCity():
    city = getCity()
    if city[:4] == 'KOTA' or city[:4] == 'KAB.':
        return city[5:].upper()
    return city.upper()
def getBirthPlaceDate_r():
    c = random.choice([1,2,3,4])
    if c == 1: 
        return 'Tempat/Tgl Lahir'
    elif c == 2:
        return f'Tempat/Tgl Lahir : {getShortCity()}'
    elif c == 3:
        return getDate()
    else:
        return f'Tempat/Tgl Lahir : {getShortCity()}, {getDate()}'
def getSex():
    return f': {random.choice(possible_sexes)}'
def getSex_r():
    c = random.choices([1,2,3], weights=[1,100,100],k=1)[0]
    if c == 1: 
        return f'Jenis Kelamin'
    elif c == 2:
        return getSex()
    else: 
        return f'Jenix Kelamin    {getSex()}'
def getBlood():
    return random.choice(blood_types)
def getBlood_r():
    c = random.choices([1,2,3], weights=[1,100,100],k=1)[0]
    if c == 1: 
        return f'Gol. Darah'
    elif c == 2:
        return getBlood()
    else: 
        return f'Gol. Darah : {getBlood()}'
def getSexBlood_r():
    c = random.choice([1,2,3])
    if c == 1: 
        return getSex_r()
    elif c == 2:
        return getBlood_r()
    else:
        return f'Jenis Kelamin    {getSex()}     Gol. Darah : {getBlood()}'
def getStreetName():
    street = random.choice(indoneasia_street_names)
    street = street.replace('Jalan Kompleks', 'Komp.')
    if bool(random.getrandbits(1)): 
        street = street.replace('Jalan', 'JL')
    else:
        street = street.replace('Jalan', 'JL.')
    # ToDo: abbreviation maybe
    # ToDo: add numbers
    return street.upper()
def getStreetName_r():
    c = random.choices([1,2,3], weights=[50,50,1],k=1)[0]
    if c == 1: 
        return f'Alamat'
    elif c == 2:
        return getStreetName()
    else: 
        return f'Alamat   : {getStreetName()}'
def getRTRW():
    s = ': 0'
    if bool(random.getrandbits(1)): 
        s += '0'
    else:
        s += '1'
    s += random.choice(string.digits[1:])
    s += '/'
    s += '0'
    if bool(random.getrandbits(1)): 
        s += '0'
    else:
        s += '1'
    s += random.choice(string.digits[1:])
    return s
def getRTRW_r():
    c = random.choices([1,2,3], weights=[1,100,100],k=1)[0]
    if c == 1: 
        return f'RT/RW'
    elif c == 2:
        return getRTRW()
    else: 
        return f'RT/RW   {getRTRW()}'
def getKelDesa():
    return f': {random.choice(sub_districts_of_indonesia)}'
def getKelDesa_r():
    c = random.choices([1,2,3], weights=[1,100,100],k=1)[0]
    if c == 1: 
        return f'Kel/Desa'
    elif c == 2:
        return getKelDesa()
    else: 
        return f'Kel/Desa     {getKelDesa()}'
def getKecamatan():
    return f': {random.choice(districts_of_indonesia)}'
def getKecamatan_r():
    c = random.choices([1,2,3], weights=[1,100,100],k=1)[0]
    if c == 1: 
        return f'Kecamatan'
    elif c == 2:
        return getKecamatan()
    else: 
        return f'Kecamatan     {getKecamatan()}'
# ToDo: add native religion like in Bandung
def getReligion():
    return f': {random.choice(possible_religions)}'
def getReligion_r():
    c = random.choices([1,2,3], weights=[50,50,1],k=1)[0]
    if c == 1: 
        return f'Agama'
    elif c == 2:
        return getReligion()
    else: 
        return f'Agama     {getReligion()}'
def getMaritalStatus():
    return f': {random.choice(possible_marital_status)}'
def getMaritalStatus_r():
    c = random.choices([1,2,3], weights=[1,100,100],k=1)[0]
    if c == 1: 
        return f'Status Perkawinan'
    elif c == 2:
        return getMaritalStatus()
    else: 
        return f'Status Perkawinan   {getMaritalStatus()}'

def getExpiryDate():
    if random.choices([1,2], weights=[100, 1], k=1)[0]:
        return getDate()
    else:
        return 'SEUMUR HIDUP'
def getExpiryDate_r():
    c = random.choices([1,2,3], weights=[1,100,100],k=1)[0]
    if c == 1: 
        return f'Berlaku Hingga'
    elif c == 2:
        return getExpiryDate()
    else: 
        return f'Berlaku Hingga   : {getExpiryDate()}'

def race():
    return f'Kewarganegaraan: WNI'

def getETKP():
    f = random.choice([getProvince, getCity, getName_r, getBirthPlaceDate_r, getSexBlood_r, getStreetName_r, getRTRW_r, getKelDesa_r, getKecamatan_r, getReligion_r, getMaritalStatus_r, getExpiryDate_r, race])
    return f()

if __name__ == "__main__":
    with open('id_NIK_train.txt', 'w') as f:
        for _ in range(64000):
            f.write(f'{getNIK_r()}\n')
    with open('id_NIK_test.txt', 'w') as f:
        for _ in range(16000):
            f.write(f'{getNIK_r()}\n')
    with open('id_etkp_train.txt', 'w') as f:
        for _ in range(64000):
            f.write(f'{getETKP()}\n')
    with open('id_etkp_test.txt', 'w') as f:
        for _ in range(16000):
            f.write(f'{getETKP()}\n')
                        