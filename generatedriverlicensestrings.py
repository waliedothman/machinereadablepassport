from corpus.givennames import *
from corpus.surnames import *
import random, string


possible_months = ["0" + str(x) for x in range(1,10)] + ["10", "11", "12"]
possible_days = possible_months + [str(x) for x in range(13, 32)]
possible_years = [str(x) for x in range(1920, 2005)]
possible_number_of_names = [x + 2 for x in range(2)]

def getName():
    name = random.choice(given_names)
    for _ in range(random.choice(possible_number_of_names) - 1):
        name = f'{name} {random.choice(surnames)}'
    return f':{name}'
def getNameString():
    return f'Nama :{getName()}'
def getDate():
    return '-'.join([random.choice(possible_days), random.choice(possible_months), random.choice(possible_years)])
def getBirthDate_r():
    return f'Tgl.Lahir   : {getDate()}'

def getETKP():
    # f = random.choice([getNameString, getBirthDate_r])
    # return f()
    return getBirthDate_r()

if __name__ == "__main__":
    with open('drivers_license_train.txt', 'w') as f:
        for _ in range(64000):
            f.write(f'{getETKP()}\n')
    with open('drivers_license_test.txt', 'w') as f:
        for _ in range(16000):
            f.write(f'{getETKP()}\n')
                        